# Encryption Project

The main approach that will be used is to remind myself of the programming in python and also to create an encryption and decryption teaching software.

## Name
Encryption and Decryption teaching software.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

**TO BE DONE**

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

**TO BE DONE**

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

**TO BE DONE**

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

**TO BE DONE**

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

**TO BE DONE**

## Authors and acknowledgment
Gytis Valatkevicius - PTI education student, UCL Odense.

## License
Project is open-source and can be used in any way shape or form.

## Project status
The project is made for a university semester project assignment, mostly linking to the theory learned in 1st semester.  
I may continue working on the project but there is no planned application besides remembering coding in python and learning more about encryption.
