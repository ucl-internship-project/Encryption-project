# A simple To-Do list made using tkinter
# Made by Gytis Valatkevicius for UCL specialization course during PTI education
# My Project's gitlab: https://gitlab.com/ucl-internship-project/Encryption-project

import pickle
import sys
from cryptography.fernet import Fernet
import tkinter
import tkinter.messagebox

# Make a root window for the app
root = tkinter.Tk()
root.title("Specialization")
root.configure(bg="white")
root.geometry("400x600")

# Create an empty list to store text inside Listbox
tasks = []

# generate a key for encryption and decryption
key = Fernet.generate_key()

# Instance the Fernet class with the key
fernet = Fernet(key)

# List control functions
def clear():
    confirmed = tkinter.messagebox.askyesno(title="Warning!", message="Do you want to delete all text?")
    if confirmed == True:
        listbox_tasks.delete(0, "end")
        print("Text cleared")
    else:
        pass


def exit():
    confirmed = tkinter.messagebox.askyesno(title="Warning!", message="Do you want to exit without saving?")
    if confirmed == True:
        sys.exit("Exiting by request")
    else:
        pass


# Task functions
def add():
    text = entry_task.get()
    if text != "":
        listbox_tasks.insert(tkinter.END, str("Entered text:"))
        listbox_tasks.insert(tkinter.END, text)
        entry_task.delete(0, tkinter.END)
        # Encoding process
        encMessage = fernet.encrypt(text.encode())
        listbox_tasks.insert(tkinter.END, str("Encrypted text:"))
        listbox_tasks.insert(tkinter.END, encMessage)
        # Decoding process
        decMessage = fernet.decrypt(encMessage).decode()
        listbox_tasks.insert(tkinter.END, str("Decrypted text:"))
        listbox_tasks.insert(tkinter.END, decMessage)
        print("Added text")
        listbox_tasks.insert(tkinter.END, str("Used Fernet decryption key:"))
        listbox_tasks.insert(tkinter.END, key)
    else:
        tkinter.messagebox.showwarning(title="Warning, unintentional use.",
                                           message="You need to enter text to decrypt.")
        print("Decrypting text failed")


def delete():
    try:
        # Remove the currently selected item
        task_index = listbox_tasks.curselection()[0]
        listbox_tasks.delete(task_index)
        print("Deleting text")
    except:
        tkinter.messagebox.showwarning(title="Warning, unintentional use.",
                                           message="You need to select text to delete.")
        print("Deleting text failed")


def load():
    try:
        tasks = pickle.load(open("Encrypted_text.dat", "rb"))
        listbox_tasks.delete(0, tkinter.END)
        for task in tasks:
            listbox_tasks.insert(tkinter.END, task)
        print("Loaded text successfully")
    except:
        tkinter.messagebox.showwarning(title="Warning, unintentional use.",
                                           message="Loading of the text has failed! Cannot find .dat file in root directory!")
        print("Loading text failed")


def save():
    try:
        tasks = listbox_tasks.get(0, listbox_tasks.size())
        # Write tasks to file saved_tasks.dat in root directory
        pickle.dump(tasks, open("Encrypted_text.dat", "wb"))
        print("Saved text successfully")
    except:
        tkinter.messagebox.showwarning(title="Warning, unintentional use.",
                                           message="Saving of the text has failed!")
        print("Saving text failed")


# Create a GUI
# Description
label_tasks = tkinter.Label(root, text="Encryption/Decryption software", bg="white")
label_tasks.pack()

label_tasks = tkinter.Label(root, text="*Unsuitable for large text files*", bg="white")
label_tasks.pack()

label_tasks = tkinter.Label(root, text="Made by Gytis Valatkevicius", bg="white")
label_tasks.pack()

label_tasks = tkinter.Label(root, text="Version: v2", bg="white")
label_tasks.pack()

# Frame
frame_tasks = tkinter.Frame(root)
frame_tasks.pack()

# Listbox
listbox_tasks = tkinter.Listbox(frame_tasks, height=10, width=100)
listbox_tasks.pack(side=tkinter.LEFT)

# Entry window
entry_task = tkinter.Entry(root, width=60, bg="#E5E5E5")
entry_task.pack()

# Task buttons
label_tasks = tkinter.Label(root, text="Task buttons", bg="#BFBFBF", pady="8")
label_tasks.pack()

button_add_task = tkinter.Button(root, text="Add text", fg="green", bg="white", width=20, command=add)
button_add_task.pack()

button_delete_task = tkinter.Button(root, text="Delete text", fg="red", bg="white", width=20, command=delete)
button_delete_task.pack()

button_clear_task = tkinter.Button(root, text="Clear text window", fg="red", bg="white", width=20, command=clear)
button_clear_task.pack()

button_exit_task = tkinter.Button(root, text="Exit encryption software", fg="#BFBFBF", bg="white", width=20, command=exit)
button_exit_task.pack()

button_load_tasks = tkinter.Button(root, text="Load text", fg="green", bg="white", width=20, command=load)
button_load_tasks.pack()

button_save_tasks = tkinter.Button(root, text="Save text", fg="green", bg="white", width=20, command=save)
button_save_tasks.pack()

# Mainloop for running the list
root.mainloop()